from kivy.app import App
from model import Model
from view import View
from kivy.core.window import Window
Window.size = (500, 600)

class Controller():
    def __init__(self):
        self.model=Model()
        self.view=View(self)

    def inser(self,widget):
            try:
                if widget.text not in ["CE","C","=","+/-"]:
                    self.view.inp.text+=widget.text
                elif widget.text=="=":
                    if self.view.inp.text[0]=="0":
                        self.view.inp.text=self.view.inp.text[1:]
                    else:
                        pass
                    self.view.rslt.text=str(eval(self.view.inp.text))
                    self.view.inp.text=""
                elif widget.text=="CE":
                    self.view.inp.text=""
                elif widget.text=="C":
                    self.view.inp.text=self.view.inp.text[:len(self.view.inp.text)-1]
                elif widget.text== "+/-":
                    if self.view.inp.text[0]=="-":
                        self.view.inp.text=self.view.inp.text[1:]
                    elif self.view.inp.text[0]!="-":
                        self.view.inp.text="-"+self.view.inp.text
            except:
                self.view.information()
                self.view.inp.text=""

class Calculator(App):
    def build(self):
        return Controller().view

if __name__=="__main__":
    Calculator().run()
