from kivy.uix.label import Label
from kivy.uix.button import Button
from kivy.uix.boxlayout import BoxLayout
from kivy.uix.gridlayout import GridLayout
from kivy.uix.popup import Popup
from kivy.uix.textinput import TextInput
class View(BoxLayout):
    def __init__(self,controller,**kwargs):
        super(View,self).__init__(**kwargs)
        self.controller=controller
        self.btnSymbol=["%","CE","C","/","7","8","9","*","4","5","6","-","1","2","3","+","+/-","0",".","="]
        self.list_btn=[]
        self.orientation="vertical"
        self.mygrid=GridLayout(cols=4,rows=5)
        self.zone_affichage_resulat()
        self.creationbutton()
        self.add_widget(self.mygrid)

    def creationbutton(self):
        for i,j in enumerate(self.btnSymbol):
            self.list_btn.append(Button(text=j, font_size=30, size_hint=(.2, 2)))
            self.list_btn[i].bind(on_press=self.controller.inser)
            self.mygrid.add_widget(self.list_btn[i])

    def zone_affichage_resulat(self,*args):
        self.inp=TextInput(text="",halign="right",font_size=35,size_hint=(1,.15 ),multiline=False,disabled=True)
        self.add_widget(self.inp)
        self.rslt = Label(text="", halign="right", font_size=35, size_hint=(1, .15))
        self.rslt.bind(size=self.rslt.setter("text_size"))
        self.add_widget(self.rslt)

    def information(self,*args):
        self.info=BoxLayout(orientation="vertical")
        self.info.add_widget(Label(text="Syntaxe invalide",font_size=20))
        self.btn_pop=Button(text="Ok",font_size=18,color="red",size_hint=(.42,.42),pos_hint={"center_x":.5})
        self.info.add_widget(self.btn_pop)
        self.popinfo=Popup(title="info",content=self.info,size_hint=(None,None),
                       size=(200,200),title_color="green",auto_dismiss=False,
                       title_align="center",title_size=20)
        self.btn_pop.bind(on_press=self.popinfo.dismiss)
        self.popinfo.open()


